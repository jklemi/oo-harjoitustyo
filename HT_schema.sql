CREATE TABLE automaatti (
"postoffice"		VARCHAR(50)	PRIMARY KEY,
"code" 			VARCHAR(10),
"city"			VARCHAR(50),
"address"		VARCHAR(50),
"availability"		VARCHAR(50),
"lat"			FLOAT,
"lng"			FLOAT
);

CREATE TABLE esine (
"id"			INTEGER		PRIMARY KEY AUTOINCREMENT NOT NULL,
"nimi"			VARCHAR(100)	NOT NULL,
"koko"			FLOAT,
"paino"			FLOAT,
"hajoava"		BOOLEAN		DEFAULT false,
"hajonnut"		BOOLEAN		DEFAULT false,
"paketissa"		BOOLEAN		DEFAULT false,

CHECK (koko>0.0),
CHECK (paino>0.0)
);

CREATE TABLE reitti (
"id"			INTEGER		PRIMARY KEY AUTOINCREMENT NOT NULL,
"nimi"			VARCHAR(100),
"pituus"		INTEGER		DEFAULT 0,
"lahtopiste"		VARCHAR(100),
"saapumispiste"		VARCHAR(100)
);

CREATE TABLE paketti (
"koodi"			INTEGER		PRIMARY KEY AUTOINCREMENT NOT NULL,
"luokka"		INTEGER		DEFAULT 1,
"lahetetty"		BOOLEAN		DEFAULT false,
"automaatti"		VARCHAR(50),
"kohdeautomaatti"	VARCHAR(50),
"esineid"		INTEGER,
"reittiid"		INTEGER,

FOREIGN KEY (automaatti) REFERENCES automaatti (nimi),
FOREIGN KEY (esineid) REFERENCES esine (id) ON DELETE CASCADE,
FOREIGN KEY (reittiid) REFERENCES reitti (id) ON DELETE CASCADE
);

CREATE TABLE "1lk_paketti" (
"koodi"			INTEGER		PRIMARY KEY,
"matkaraja"		INTEGER		DEFAULT 150,
"hajoaa"		BOOLEAN		DEFAULT true,

FOREIGN KEY (koodi) REFERENCES paketti (koodi)
);

CREATE TABLE "2lk_paketti" (
"koodi"			INTEGER		PRIMARY KEY,
"kokoraja"		FLOAT		DEFAULT 0.5,
"hajoaa"		BOOLEAN		DEFAULT false,

FOREIGN KEY (koodi) REFERENCES paketti (koodi)
);

CREATE TABLE "3lk_paketti" (
"koodi"			INTEGER		PRIMARY KEY,
"koon_alaraja"		FLOAT		DEFAULT 0.5,
"painon_alaraja"	FLOAT		DEFAULT 10.0,
"hajoaa"		BOOLEAN		DEFAULT true,

FOREIGN KEY (koodi) REFERENCES paketti (koodi)
);

CREATE TABLE "loki" (
"id"		INTEGER	PRIMARY KEY,
"aikaleima"	DATE,
"tyyppi"	VARCHAR(30),
"kohde"		VARCHAR(30)
);

CREATE VIEW "varaston_paketit" (pakettikoodi, pakettiluokka, toimitettu, varastoautomaatti, kohdeautomaatti, matkan_pituus, esine, esine_hajonnut) AS
SELECT p.koodi, p.luokka, p.lahetetty, p.automaatti, p.kohdeautomaatti, r.pituus, e.nimi, e.hajonnut
FROM paketti p
INNER JOIN reitti r ON p.reittiid = r.id
INNER JOIN esine e ON p.esineid = e.id
GROUP BY p.koodi;
