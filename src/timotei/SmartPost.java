/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

/**
 *
 * @author juuso
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private GeoPoint gp;
    
    public SmartPost(String code, String city, String address,
            String availability, String postoffice, float lat, float lng) {
        this.code = code;
        this.city = city;
        this.address = address;
        this.availability = availability;
        this.postoffice = postoffice;
        gp = new GeoPoint(lat, lng);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public void setPostoffice(String postoffice) {
        this.postoffice = postoffice;
    }
    
    public float getLat(){
        return gp.getLat();
    }
    
    public float getLng(){
        return gp.getLng();
    }

    private class GeoPoint{
        private float lat;
        private float lng;
        
        public GeoPoint(float lat, float lng){
            this.lat = lat;
            this.lng = lng;
        }
        
        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }

        public float getLng() {
            return lng;
        }

        public void setLng(float lng) {
            this.lng = lng;
        }
    }
}
