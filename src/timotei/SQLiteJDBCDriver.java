/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Juuso Klemi
 */
public class SQLiteJDBCDriver {
    private Connection conn;
    private int reitti_id;
    private int esine_id;
    private int paketti_id;
    
    private static SQLiteJDBCDriver instance;
    
    private SQLiteJDBCDriver(){
        reitti_id = 0;
        esine_id = 0;
        paketti_id = 0;
        connect();
    }
    
    public static SQLiteJDBCDriver getInstance(){
        if(instance == null){
            instance = new SQLiteJDBCDriver();
        }
        return instance;
    }
    
    private void connect() {
        conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:testi.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
    }
    
    public ResultSet getViewInfo() throws SQLException{
        String SQL = "SELECT * FROM varaston_paketit";
        ResultSet rs = conn.createStatement().executeQuery(SQL);
        return rs;
    }
    
    public boolean automaattiIsEmpty(){
        try (PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM automaatti")) {
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()){
                return false;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return true;
    }
    
    public ArrayList<String> getCities(){
        ArrayList<String> cities = new ArrayList<>();
        String sql = "SELECT DISTINCT city FROM automaatti ORDER BY city";
        
        try (PreparedStatement pstmt  = conn.prepareStatement(sql)){
            // execute query
            ResultSet rs  = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                cities.add(rs.getString("city"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return cities;
    }
    
    public int getItemID() {
        try (PreparedStatement pstmt = conn.prepareStatement("SELECT id FROM esine")) {
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                esine_id = rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return esine_id;
    }
    
    public int getRouteID() {
        try (PreparedStatement pstmt = conn.prepareStatement("SELECT id FROM reitti")) {
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                reitti_id = rs.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return reitti_id;
    }
    
    public int getPackageID() {
        try (PreparedStatement pstmt = conn.prepareStatement("SELECT koodi FROM paketti")) {
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                paketti_id = rs.getInt("koodi");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return paketti_id;
    }
    
    public void clearTables() {
        clearPaketti();
        clearAutomaatti();
        clearEsine();
        clearReitti();
    }
    
    public void clearPaketti() {
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM paketti")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM '1lk_paketti'")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM '2lk_paketti'")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM '3lk_paketti'")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void clearAutomaatti() {
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM automaatti")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void clearEsine() {
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM esine")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void clearReitti() {
        try (PreparedStatement pstmt = conn.prepareStatement("DELETE FROM reitti")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insertSmp(String code, String city, String address, 
            String availability, String postoffice, float lat, float lng) {
        String sql = "INSERT INTO automaatti(code, city, address, availability,"
                + " postoffice, lat, lng) VALUES(?,?,?,?,?,?,?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, code);
            pstmt.setString(2, city);
            pstmt.setString(3, address);
            pstmt.setString(4, availability);
            pstmt.setString(5, postoffice);
            pstmt.setFloat(6, lat);
            pstmt.setFloat(7, lng);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insertItem(int id, String name, float size, float mass, boolean breakable, boolean broken, boolean packaged) {
        String sql = "INSERT INTO esine(id, nimi, koko, paino, hajoava, hajonnut, paketissa) VALUES(?,?,?,?,?,?,?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.setString(2, name);
            pstmt.setDouble(3, size);
            pstmt.setDouble(4, mass);
            pstmt.setBoolean(5, breakable);
            pstmt.setBoolean(6, broken);
            pstmt.setBoolean(7, packaged);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insertRoute(int id, String name, double length, String startAddress, String destAddress) {
        String sql = "INSERT INTO reitti(id, nimi, pituus, lahtopiste, saapumispiste) VALUES(?,?,?,?,?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.setString(2, name);
            pstmt.setDouble(3, length);
            pstmt.setString(4, startAddress);
            pstmt.setString(5, destAddress);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insertPackage(int code, int classNum, boolean sent, String postoffice, String endPostoffice, int itemID, int routeID) {
        String sql = "INSERT INTO paketti(koodi, luokka, lahetetty, automaatti, kohdeautomaatti, esineid, reittiid) VALUES(?,?,?,?,?,?,?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, code);
            pstmt.setInt(2, classNum);
            pstmt.setBoolean(3, sent);
            pstmt.setString(4, postoffice);
            pstmt.setString(5, endPostoffice);
            pstmt.setInt(6, itemID);
            pstmt.setInt(7, routeID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insert1stClass(int packageID) {
        String sql = "INSERT INTO '1lk_paketti'(koodi) VALUES(?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, packageID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insert2ndClass(int packageID) {
        String sql = "INSERT INTO '2lk_paketti'(koodi) VALUES(?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, packageID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void insert3rdClass(int packageID) {
        String sql = "INSERT INTO '3lk_paketti'(koodi) VALUES(?)";
 
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, packageID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public int getStoreageAmount() {
        int SAmount = -1;
        String sql = "SELECT COUNT(*) AS summa FROM paketti WHERE lahetetty = 0";
        
        try (PreparedStatement pstmt  = conn.prepareStatement(sql)){
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                SAmount = (rs.getInt("summa"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return SAmount;
    }
    
    public int getDeliveredAmount() {
        int DAmount = -1;
        String sql = "SELECT COUNT(*) AS summa FROM paketti WHERE lahetetty = 1";
        
        try (PreparedStatement pstmt  = conn.prepareStatement(sql)){
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                DAmount = (rs.getInt("summa"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return DAmount;
    }
    
    public String getItemFromPaketti(int koodi){
        String itemName = "default";
        String sql = "SELECT esine.nimi FROM esine INNER JOIN paketti ON paketti.esineid = esine.id WHERE paketti.koodi = ?";
        
        try (PreparedStatement pstmt  = conn.prepareStatement(sql)){
            pstmt.setInt(1, koodi);
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                itemName = (rs.getString("nimi"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return itemName;
    }
}
