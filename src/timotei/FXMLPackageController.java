/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import static timotei.FXMLDocumentController.il;
import static timotei.FXMLDocumentController.pl;
import static timotei.FXMLDocumentController.rl;
import static timotei.FXMLDocumentController.sj;
import static timotei.FXMLDocumentController.spl;

/**
 * FXML Controller class
 *
 * @author Juuso Klemi
 */
public class FXMLPackageController implements Initializable {
    
    
    @FXML
    private ComboBox<Item> itemsComboBox;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemMassField;
    @FXML
    private CheckBox itemBreaksBox;
    @FXML
    private RadioButton firstClassButton;
    @FXML
    private ToggleGroup pakettiluokka;
    @FXML
    private RadioButton secondClassButton;
    @FXML
    private RadioButton thirdClassButton;
    @FXML
    private Button classInfoButton;
    @FXML
    private ComboBox<String> startCityBox;
    @FXML
    private ComboBox<SmartPost> startPOBox;
    @FXML
    private ComboBox<String> destCityBox;
    @FXML
    private ComboBox<SmartPost> destPOBox;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button cancelButton;
    @FXML
    private WebView hiddenWebView;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateBoxes();
        
        itemsComboBox.setConverter(new StringConverter<Item>() {
        @Override
        public String toString(Item object) {
            return object.getId() + ". " + object.getName();
        }

        @Override
        public Item fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        });
        
        startPOBox.setConverter(new StringConverter<SmartPost>() {
        @Override
        public String toString(SmartPost object) {
            return object.getPostoffice();
        }

        @Override
        public SmartPost fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        });
        
        destPOBox.setConverter(new StringConverter<SmartPost>() {
        @Override
        public String toString(SmartPost object) {
            return object.getPostoffice();
        }

        @Override
        public SmartPost fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        });
        
        hiddenWebView.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }
    
    public void populateBoxes() {
        //initialize items ComboBox
        
        itemsComboBox.getItems().clear();
        for (Item i : il.getItemList()) {
            if (!i.isPackaged()) {
                itemsComboBox.getItems().add(i);
            }
        }
        
        //initialize city boxes
        startCityBox.getItems().clear();
        destCityBox.getItems().clear();
        ArrayList<String> cities = sj.getCities();
        for (String c : cities){
            startCityBox.getItems().add(c);
            destCityBox.getItems().add(c);
        }
        
    }

    @FXML
    private void createPackage(ActionEvent event) {
        String startAddress;
        String destAddress;
        int classNum;
        int routeID;
        int itemID = 0;
        double routeDistance;
        
        String itemName = "defaultName";
        float itemSize;
        float itemMass;
        boolean breakable;
        
        //check class selection
        if (firstClassButton.isSelected()) {
            classNum = 1;
        }
        else if (secondClassButton.isSelected()){
            classNum = 2;
        }
        else if (thirdClassButton.isSelected()) {
            classNum = 3;
        }
        else {
            System.out.println("Et ole valinnut luokkaa.");
            return;
        }
            
        //check item selection
        if (itemsComboBox.getSelectionModel().isEmpty() && itemNameField.getText().isEmpty()) {
            System.out.println("Et ole valinnut esinettä.");
            return;
        }
        
        if (startPOBox.getSelectionModel().isEmpty()) {
            System.out.println("Et ole valinnut lähtöautomaattia.");
            return;
        }
        
        if (destPOBox.getSelectionModel().isEmpty()) {
            System.out.println("Et ole valinnut kohdeautomaattia.");
            return;
        }
        
        //get start address
        startAddress = startPOBox.getValue().getAddress();
        
        //get destination address
        destAddress = destPOBox.getValue().getAddress();
        
        
        //get route distance
        ArrayList<String> pisteet = new ArrayList<>();
        String startLat = "default";
        String startLng = "default";
        String endLat = "default";
        String endLng = "default";
        for (SmartPost smp : spl.getSmpList()){
            if (smp == startPOBox.getValue()) {
                startLat = Float.toString(smp.getLat());
                startLng = Float.toString(smp.getLng());
            }
            if (smp == destPOBox.getValue()) {
                endLat = Float.toString(smp.getLat());
                endLng = Float.toString(smp.getLng());
            }
        }
        
        pisteet.add(startLat);
        pisteet.add(startLng);
        pisteet.add(endLat);
        pisteet.add(endLng);
        
        routeDistance = (double)hiddenWebView.getEngine().executeScript("document.createPath("+pisteet+", 'blue', 1)");
        hiddenWebView.getEngine().executeScript("document.deletePaths()");
        

        if (!itemNameField.getText().isEmpty()){
            //create new item based on user input
            itemName = itemNameField.getText();
            itemSize = 1.0f;
            itemMass = Float.parseFloat(itemMassField.getText());
            breakable = itemBreaksBox.isSelected();
            
            //calculate size from "cm*cm*cm"
            String itemSizeString = itemSizeField.getText();
            String[] measurements = itemSizeString.split("\\*");
            for (String measurement : measurements) 
                itemSize = itemSize * Float.parseFloat(measurement);
            itemSize = itemSize / 1000000.0f;
            
        } else {
            //get item information from ComboBox
            itemID = itemsComboBox.getValue().getId();
            itemSize = itemsComboBox.getValue().getSize();
            itemMass = itemsComboBox.getValue().getMass();
            breakable = itemsComboBox.getValue().isBreakable();
        }
        
        //check class constraints
        if (classNum==1 && routeDistance > 150.0) {
            System.out.println("1. luokassa ei voi lähettää esinettä yli 150 km matkalle.");
            return;
        }
        else if (classNum==2 && itemSize > 0.5f) {
            System.out.println("2. luokassa ei voi lähettää noin isoa pakettia.");
            return;
        }
        else if (classNum==3 && itemSize < 0.5f) {
            System.out.println("3. luokassa ei voi lähettää noin pientä pakettia.");
            return;
        }
        else if (classNum==3 && itemMass < 10.0f) {
            System.out.println("3. luokassa ei voi lähettää noin kevyttä pakettia.");
            return;
        }
        
        if (!itemNameField.getText().isEmpty()) {
            itemID = il.addItem(itemName, itemSize, itemMass, breakable);
            il.updateDB();
        }
        
        il.packageItem(itemID);
        
         //add route to table
        routeID = rl.addRoute(startCityBox.getValue()+"-"+destCityBox.getValue(), routeDistance, startAddress, destAddress);
        rl.updateDB();
        
         //insert package to correct tables and packagelist
        if (classNum==1) {
            pl.addAPck(classNum, startPOBox.getValue().getPostoffice(), destPOBox.getValue().getPostoffice(), itemID, routeID);
            pl.updateDB();
        } else if (classNum==2){
            pl.addBPck(classNum, startPOBox.getValue().getPostoffice(), destPOBox.getValue().getPostoffice(), itemID, routeID);
            pl.updateDB();
        } else if (classNum==3) {
            pl.addCPck(classNum, startPOBox.getValue().getPostoffice(), destPOBox.getValue().getPostoffice(), itemID, routeID);
            pl.updateDB();
        }
        
        //close package creation window
            Stage stage = (Stage)cancelButton.getScene().getWindow();
            stage.close();
    }

    @FXML
    private void cancelEvent(ActionEvent event) {
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void startUpdatePO(ActionEvent event) {
        String chosenCity = startCityBox.getValue();
        
        startPOBox.getItems().clear();
        for (SmartPost smp : spl.getSmpList()){
            if (smp.getCity().equals(chosenCity)) {
                startPOBox.getItems().add(smp);
            }
        }
    }

    @FXML
    private void destUpdatePO(ActionEvent event) {
        String chosenCity = destCityBox.getValue();
        
        destPOBox.getItems().clear();
        for (SmartPost smp : spl.getSmpList()){
            if (smp.getCity().equals(chosenCity)) {
                destPOBox.getItems().add(smp);
            }
        }
    }

    @FXML
    private void displayClassInfo(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLClassInfo.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Luokkatiedot");
        stage.setScene(new Scene(root));
        stage.show();
    }
    
    
    
}
