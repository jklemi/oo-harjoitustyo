/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Juuso Klemi
 */
public class FXMLDocumentController implements Initializable {
    public static SQLiteJDBCDriver sj = SQLiteJDBCDriver.getInstance();
    public static SmartPostList spl = SmartPostList.getInstance();
    public static PackageList pl = PackageList.getInstance();
    public static ItemList il = ItemList.getInstance();
    public static RouteList rl = RouteList.getInstance();
    
    @FXML
    private WebView webViewMap;
    @FXML
    private Font x2;
    @FXML
    private Insets x3;
    @FXML
    private ComboBox<String> citySelectionBox;
    @FXML
    private Insets x1;
    @FXML
    private Insets x4;
    @FXML
    private Button drawPointsButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button clearRoutesButton;
    @FXML
    private Button exitButton;
    @FXML
    private ComboBox<Package> packagesBox;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button updatePackagesButton;
    @FXML
    private Button packageInfoButton;
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        packagesBox.setConverter(new StringConverter<Package>() {
        @Override
        public String toString(Package object) {
            return "Luokka " + object.getClassNum() + " Paketti " + object.getCode();
        }
        @Override
        public Package fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        });
        
        //Load index.html to WebView
        webViewMap.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //Read XML-file from web site
        URL url2 = null;
        try {
            url2 = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader in;
        String inputLine, total="";
        try {
            in = new BufferedReader(new InputStreamReader(url2.openStream()));
            while ((inputLine = in.readLine()) != null) {
                total += inputLine + "\n";
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        XMLReader a = new XMLReader(total);
        int laskuri = a.getMap().size();
        laskuri = laskuri / 7;
        
        //if table automaatti is empty, add data to it
        for (int i=0; i<laskuri; i++) {
            spl.addSmp(
                (String)a.getMap().get("code"+i), 
                (String)a.getMap().get("city"+i), 
                (String)a.getMap().get("address"+i), 
                (String)a.getMap().get("availability"+i), 
                (String)a.getMap().get("postoffice"+i), 
                Float.parseFloat((String)a.getMap().get("lat"+i)), 
                Float.parseFloat((String)a.getMap().get("lng"+i))
            );
        }
        if (sj.automaattiIsEmpty()){
            spl.updateDB();
        }
        
        updateCities();
        
        
        il.addItem("Antiikkinen vaasi", 0.4f, 8.5f, true);
        il.addItem("Kahvakuula", 0.5f, 16.0f, false);
        il.addItem("Korttipakka", 0.05f, 0.01f, false);
        il.addItem("Peliohjain", 0.25f, 1.5f, true);
        il.addItem("Työkalusarja", 0.7f, 7.2f, false);
        il.updateDB();
        
        //initialize packages box
        updatePackages();
    }
    
    public void updatePackages() {
        packagesBox.getItems().clear();
        
        for (APackage a : pl.getAPckList()){
            if (!a.isSent())
                packagesBox.getItems().add(a);
        }
        for (BPackage b : pl.getBPckList()){
            if (!b.isSent())
                packagesBox.getItems().add(b);
        }
        for (CPackage c : pl.getCPckList()){
            if (!c.isSent())
                packagesBox.getItems().add(c);
        }
        
    }
    
        
    public void updateCities() {
        citySelectionBox.getItems().clear();
        
        ArrayList<String> cities = sj.getCities();
        for (String c : cities){
            citySelectionBox.getItems().add(c);
        }
    }

    @FXML
    private void drawPoints(ActionEvent event) {
        String chosenCity = citySelectionBox.getValue();

        for (SmartPost smp : spl.getSmpList()) {
            if (smp.getCity().equals(chosenCity)) {
            webViewMap.getEngine().executeScript("document.goToLocation('"+smp.getAddress()+", "+smp.getCode()+" "
                    +smp.getCity()+"', '"+smp.getPostoffice()+" | Aukioloajat: "+smp.getAvailability()+"', 'red')");
            }
        }
        
    }

    @FXML
    private void openPackageWindow(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Luo paketti");
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    private void clearRoutes(ActionEvent event) {
        webViewMap.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void exitButtonAction(ActionEvent event) {
        sj.clearEsine();
        sj.clearPaketti();
        sj.clearReitti();
        sj.clearTables();
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sendPackage(ActionEvent event) {
        ArrayList<String> pisteet = new ArrayList<>();
        String color = "purple";
        int packageCode = packagesBox.getValue().getCode();
        int packageClass = packagesBox.getValue().getClassNum();
        String startPO = packagesBox.getValue().getPostoffice();
        String endPO = packagesBox.getValue().getDest_postoffice();
        String startLat = "default";
        String startLng = "default";
        String endLat = "default";
        String endLng = "default";
        
        
        //determine draw color
        switch (packageClass) {
            case 1:
                color = "red";
                break;
            case 2:
                color = "green";
                break;
            case 3:
                color = "blue";
                break;
            default:
                System.out.println("Draw color error");
                break;
        }
        
        //get point coordinates
        for (SmartPost smp : spl.getSmpList()){
            if (smp.getPostoffice().equals(startPO)) {
                startLat = Float.toString(smp.getLat());
                startLng = Float.toString(smp.getLng());
            }
            if (smp.getPostoffice().equals(endPO)) {
                endLat = Float.toString(smp.getLat());
                endLng = Float.toString(smp.getLng());
            }
        }
        
        pisteet.add(startLat);
        pisteet.add(startLng);
        pisteet.add(endLat);
        pisteet.add(endLng);
        
        //draw on map
        webViewMap.getEngine().executeScript("document.createPath("+pisteet+", '"+color+"', "+packageClass+")");
        
        //send package
        pl.sendPackage(packageClass, packageCode);
        
        updatePackages();
    }

    @FXML
    private void updatePackagesEvent(ActionEvent event) {
        updatePackages();
    }

    @FXML
    private void openPackageView(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackageView.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Pakettien tiedot");
        stage.setScene(new Scene(root));
        stage.show();
    }

}
