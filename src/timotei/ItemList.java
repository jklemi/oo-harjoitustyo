/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.util.ArrayList;
import static timotei.FXMLDocumentController.sj;

/**
 *
 * @author Juuso Klemi
 */
public class ItemList {
    private static ItemList instance;
    private ArrayList<Item> itemList;
    private int newID;
    
    private ItemList(){
        itemList = new ArrayList<>();
        newID = sj.getItemID() + 1;
    }
    
    public static ItemList getInstance(){
        if(instance == null){
            instance = new ItemList();
        }
        return instance;
    }
    
    public int addItem(String name, float size, float mass, boolean breakable){
        itemList.add(new Item(newID, name, size, mass, breakable));
        newID++;
        return newID - 1;
    }
    
    public void breakItem(int id){
        for (Item i : itemList){
            if(i.getId() == id && i.isBreakable() == true){
                i.setBroken(true);
                System.out.println("Esine hajosi matkan aikana.");
            }
        }
        updateDB();
    }
    
    public void packageItem(int id){
        for (Item i : itemList){
            if(i.getId() == id){
                i.setPackaged(true);
            }
        }
        updateDB();
    }
    
    public void updateDB(){
        sj.clearEsine();
        for (Item i : itemList){
            sj.insertItem(i.getId(), i.getName(), i.getSize(), i.getMass(), i.isBreakable(), i.isBroken(), i.isPackaged());
        }
    }
    
    public ArrayList<Item> getItemList() {
        return itemList;
    }
}
