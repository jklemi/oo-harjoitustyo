/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author p1333
 */
public class XMLReader {
    private Document doc;
    private HashMap<String, String> map;
    
    public XMLReader(String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            map = new HashMap();
            parseCurrentData();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("item");
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            map.put("code" + i, getTextContent("postalcode", e));
            map.put("city" + i, getTextContent("city", e));
            map.put("address" + i, getTextContent("address", e));
            map.put("availability" + i, getTextContent("availability", e));
            map.put("postoffice" + i, getTextContent("name", e));
            map.put("lat" + i, getTextContent("lat", e));
            map.put("lng" + i, getTextContent("lng", e));
        }
    }
        
    private String getTextContent(String tag, Element e) {
        return (e.getElementsByTagName(tag).item(0).getTextContent()); 
    }
    
    public HashMap<String, String> getMap() {
        return map;
    }
}
