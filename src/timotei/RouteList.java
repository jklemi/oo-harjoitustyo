/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.util.ArrayList;
import static timotei.FXMLDocumentController.sj;

/**
 *
 * @author Juuso Klemi
 */
public class RouteList {
    private static RouteList instance;
    private ArrayList<Route> routeList;
    private int newID;
    
    private RouteList(){
        routeList = new ArrayList<>();
        newID = sj.getRouteID() + 1;
    }
    
    public static RouteList getInstance(){
        if(instance == null){
            instance = new RouteList();
        }
        return instance;
    }
    
    public int addRoute(String name, double length, String startPoint, String endPoint){
        routeList.add(new Route(newID, name, length, startPoint, endPoint));
        newID++;
        return newID - 1;
    }
    
    public void updateDB(){
        sj.clearReitti();
        for (Route r : routeList){
            sj.insertRoute(r.getId(), r.getName(), r.getLength(), r.getStartPoint(), r.getEndPoint());
        }
    }
    
    public ArrayList<Route> getRouteList() {
        return routeList;
    }
}
