/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

/**
 *
 * @author Juuso Klemi
 */
public class Route {
    private int id;
    private String name;
    private double length;
    private String startPoint;
    private String endPoint;

    public Route(int id, String name, double length, String startPoint, String endPoint) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }
    
    
}
