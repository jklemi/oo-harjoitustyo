/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import static timotei.FXMLDocumentController.il;

/**
 *
 * @author Juuso Klemi
 */
abstract class Package {
    protected int code;
    protected int classNum;
    protected boolean sent;
    protected String postoffice;
    protected String dest_postoffice;
    protected int item_id;
    protected int route_id;

    public Package(int code, int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id) {
        this.code = code;
        this.classNum = classNum;
        this.sent = false;
        this.postoffice = postoffice;
        this.dest_postoffice = dest_postoffice;
        this.item_id = item_id;
        this.route_id = route_id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getClassNum() {
        return classNum;
    }

    public void setClassNum(int classNum) {
        this.classNum = classNum;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public void setPostoffice(String postoffice) {
        this.postoffice = postoffice;
    }

    public String getDest_postoffice() {
        return dest_postoffice;
    }

    public void setDest_postoffice(String dest_postoffice) {
        this.dest_postoffice = dest_postoffice;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getRoute_id() {
        return route_id;
    }

    public void setRoute_id(int route_id) {
        this.route_id = route_id;
    }
    
}

class APackage extends Package {
    private int maxDistance;
    private boolean breaks;
    
    public APackage(int code, int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id) {
        super(code, classNum, postoffice, dest_postoffice, item_id, route_id);
        maxDistance = 150;
        breaks = true;
    }
    
    public void send(){
        this.sent = true;
        il.breakItem(item_id);
    }
}

class BPackage extends Package {
    private float maxSize;
    private boolean breaks;
    
    public BPackage(int code, int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id) {
        super(code, classNum, postoffice, dest_postoffice, item_id, route_id);
        maxSize = 0.5f;
        breaks = false;
    }
    
    public void send(){
        this.sent = true;
    }
}

class CPackage extends Package {
    private float minSize;
    private float minMass;
    private boolean breaks;
    
    public CPackage(int code, int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id) {
        super(code, classNum, postoffice, dest_postoffice, item_id, route_id);
        minSize = 0.5f;
        minMass = 5.0f;
        breaks = true; 
    }
    
    public void send(){
        this.sent = true;
        il.breakItem(item_id);
    }
    
}