/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.util.ArrayList;
import static timotei.FXMLDocumentController.sj;

/**
 *
 * @author Juuso Klemi
 */
public class PackageList {
    private static PackageList instance;
    private ArrayList<APackage> APckList;
    private ArrayList<BPackage> BPckList;
    private ArrayList<CPackage> CPckList;
    private int newID;
    
    private PackageList(){
        APckList = new ArrayList<>();
        BPckList = new ArrayList<>();
        CPckList = new ArrayList<>();
        newID = sj.getPackageID() + 1;
    }
    
    public static PackageList getInstance(){
        if(instance == null){
            instance = new PackageList();
        }
        return instance;
    }

    public ArrayList<APackage> getAPckList() {
        return APckList;
    }

    public ArrayList<BPackage> getBPckList() {
        return BPckList;
    }

    public ArrayList<CPackage> getCPckList() {
        return CPckList;
    }
    
    public void addAPck(int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id){
        APckList.add(new APackage(newID, classNum, postoffice, dest_postoffice, item_id, route_id));
        newID++;
    }
    
    public void addBPck(int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id){
        BPckList.add(new BPackage(newID, classNum, postoffice, dest_postoffice, item_id, route_id));
        newID++;
    }
    
    public void addCPck(int classNum, String postoffice, 
            String dest_postoffice, int item_id, int route_id){
        CPckList.add(new CPackage(newID, classNum, postoffice, dest_postoffice, item_id, route_id));
        newID++;
    }
    
    public void updateDB(){
        sj.clearPaketti();
        for (APackage a : APckList){
            sj.insertPackage(a.getCode(), a.getClassNum(), a.isSent(), a.getPostoffice(), a.getDest_postoffice(), a.getItem_id(), a.getRoute_id());
            sj.insert1stClass(a.getCode());
        }
        for (BPackage b : BPckList){
            sj.insertPackage(b.getCode(), b.getClassNum(), b.isSent(), b.getPostoffice(), b.getDest_postoffice(), b.getItem_id(), b.getRoute_id());
            sj.insert2ndClass(b.getCode());
        }
        for (CPackage c : CPckList){
            sj.insertPackage(c.getCode(), c.getClassNum(), c.isSent(), c.getPostoffice(), c.getDest_postoffice(), c.getItem_id(), c.getRoute_id());
            sj.insert3rdClass(c.getCode());
        }
    }
    
    public void sendPackage(int classNum, int code){
        if (classNum == 1) {
            for (APackage a : APckList){
                if (code == a.getCode()){
                    a.send();
                }
            }
            updateDB();
        }
        else if (classNum == 2) {
            for (BPackage b : BPckList){
                if (code == b.getCode()){
                    b.send();
                }
            }
            updateDB();
        }
        else if (classNum == 3) {
            for (CPackage c : CPckList){
                if (code == c.getCode()){
                    c.send();
                }
            }
            updateDB();
        }
        else System.out.println("Error");
    }
}
