/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

/**
 *
 * @author Juuso Klemi
 */
public class Item {
    private int id;
    private String name;
    private float size;
    private float mass;
    private boolean breakable;
    private boolean broken;
    private boolean packaged;
    
    public Item(int id, String name, float size, float mass, boolean breakable){
        this.id = id;
        this.name = name;
        this.size = size;
        this.mass = mass;
        this.breakable = breakable;
        this.broken = false;
        this.packaged = false;
    }
    
    public void breakItem() {
        if (breakable == true)
            this.broken = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public boolean isBreakable() {
        return breakable;
    }

    public void setBreakable(boolean breakable) {
        this.breakable = breakable;
    }

    public boolean isBroken() {
        return broken;
    }

    public void setBroken(boolean broken) {
        this.broken = broken;
    }

    public boolean isPackaged() {
        return packaged;
    }

    public void setPackaged(boolean packaged) {
        this.packaged = packaged;
    }
}
