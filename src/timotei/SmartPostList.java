/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.util.ArrayList;
import static timotei.FXMLDocumentController.sj;

/**
 *
 * @author Juuso
 */
public class SmartPostList {
    private static SmartPostList instance;
    private ArrayList<SmartPost> smpList;
    
    private SmartPostList(){
        smpList = new ArrayList<>();
    }
    
    public static SmartPostList getInstance(){
        if(instance == null){
            instance = new SmartPostList();
        }
        return instance;
    }
    
    public void addSmp(String code, String city, String address,
            String availability, String postoffice, float lat, float lng){
        smpList.add(new SmartPost(code, city, address, availability, postoffice, lat, lng));
    }
    
    public ArrayList<SmartPost> getSmpList() {
        return smpList;
    }
    
    public void updateDB() {
        sj.clearAutomaatti();
        for (SmartPost s : smpList){
            sj.insertSmp(s.getCode(), s.getCity(), s.getAddress(), s.getAvailability(), s.getPostoffice(), s.getLat(), s.getLng());
        }
    }
}
