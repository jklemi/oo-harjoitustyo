/*
 * Tekijä: Juuso Klemi 0520567
 * Päivitetty: 25.7.2018
 * 
 */
package timotei;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import static timotei.FXMLDocumentController.sj;

/**
 * FXML Controller class
 *
 * @author juuso
 */
public class FXMLPackageViewController implements Initializable {

    @FXML
    private TableView<ObservableList> packageTable;
    @FXML
    private Button closeTableButton;
    @FXML
    private Label storageAmount;
    @FXML
    private Label deliveredAmount;
    @FXML
    private TextField searchField;
    @FXML
    private Button searchButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buildData();
    }    
    
    public void buildData(){
        ObservableList<ObservableList> data;
        data = FXCollections.observableArrayList();
        try{
            ResultSet rs = sj.getViewInfo();

            //build table data dynamically
            for(int i=0 ; i<rs.getMetaData().getColumnCount(); i++){
                final int j = i;                
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i+1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){                    
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {                                                                                              
                        return new SimpleStringProperty(param.getValue().get(j).toString());                        
                    }                    
                });

                packageTable.getColumns().addAll(col); 
                //System.out.println("Column ["+i+"] ");
            } 

            // add data to observableList
            while(rs.next()){
                ObservableList<String> row = FXCollections.observableArrayList();
                for(int i=1 ; i<=rs.getMetaData().getColumnCount(); i++){
                    row.add(rs.getString(i));
                }
                //System.out.println("Row [1] added "+row );
                data.add(row);
            }

            //add data to tableview
            packageTable.setItems(data);
          }catch(Exception e){
              e.printStackTrace();
              System.out.println("Error on Building Data");             
          }
        
        //populate amounts
        storageAmount.setText(Integer.toString(sj.getStoreageAmount()));
        deliveredAmount.setText(Integer.toString(sj.getDeliveredAmount()));
        
        
      }

    @FXML
    private void closeTableView(ActionEvent event) {
        Stage stage = (Stage) closeTableButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void searchButtonAction(ActionEvent event) {
        String koodiString = searchField.getText();
        String itemName;
        int koodi = -1;
        try {
            koodi = Integer.parseInt(koodiString);
        } catch (NumberFormatException nfe) {
            System.out.println("Antamasi koodi ei ole luku.");
            return;
        }
        itemName = sj.getItemFromPaketti(koodi);
        searchField.setText(itemName);
    }
}